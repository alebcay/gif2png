// -*- mode: Go;-*-
module gitlab.com/esr/gif2png

require (
	golang.org/x/crypto v0.8.0
	golang.org/x/sys v0.7.0
)

go 1.13
